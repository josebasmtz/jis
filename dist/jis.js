(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("jis", [], factory);
	else if(typeof exports === 'object')
		exports["jis"] = factory();
	else
		root["jis"] = factory();
})(this, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ 138:
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
var Is = /** @class */ (function () {
    function Is() {
    }
    Is.callToString = function (arg) {
        return Object.prototype.toString.call(arg);
    };
    Is.$string = function (arg) {
        return Is.callToString(arg) === '[object String]';
    };
    Is.is = function (arg, type) {
        if (Is.$function(type)) {
            return arg instanceof type;
        }
        if (Is.$string(type)) {
            return Is.callToString(arg) === "[object " + type + "]";
        }
        return arg === type;
    };
    Is.$array = function (arg) {
        return Is.is(arg, 'Array');
    };
    Is.$null = function (arg) {
        return Is.is(arg, 'Null');
    };
    Is.$number = function (arg) {
        return Is.is(arg, 'Number');
    };
    Is.$object = function (arg) {
        return Is.is(arg, 'Object');
    };
    Is.$undefined = function (arg) {
        return Is.is(arg, 'Undefined');
    };
    Is.$boolean = function (arg) {
        return Is.is(arg, 'Boolean');
    };
    Is.$function = function (arg) {
        return Is.callToString(arg) === '[object Function]';
    };
    Is.objectIsValid = function (data, rules) {
        if (!Is.$object(data))
            throw new Error('The data parameter must be an Object');
        if (!Is.$object(rules))
            throw new Error('The rules parameter must be an Object');
        var $response = true;
        var $keys = Object.getOwnPropertyNames(rules);
        $keys.forEach(function (key) {
            var rule = rules[key];
            if (Is.$array(rule)) {
                if (rule.length < 1)
                    return;
                var parcial_1 = false;
                rule.forEach(function (_rule) {
                    parcial_1 = parcial_1 || Is.is(data[key], _rule);
                });
                return $response = $response && parcial_1;
            }
            $response = $response && Is.is(data[key], rule);
        });
        return $response;
    };
    Is.$numeric = function (arg) {
        if (Is.$number(arg)) {
            return true;
        }
        var $arg = String(arg || '');
        var regex = /^[-+]?(([0-9]+)|([0-9]*(\.[0-9]+))|([0-9]+\.))([Ee]([-+]?[0-9]+))?$/g;
        return regex.test($arg);
    };
    Is.$primitive = function (arg) {
        var validations = [
            Is.$undefined,
            Is.$null,
            Is.$boolean,
            Is.$number,
            Is.$string,
            function (arg) { return Is.is(arg, 'Symbol'); }
        ];
        return validations.some(function (item) { return item(arg); });
    };
    Is.$empty = function (arg) {
        var validations = [
            Is.$undefined,
            Is.$null,
            function (arg) { return Is.$boolean(arg) && !arg; },
            function (arg) { return Is.$number(arg) && arg === 0; },
            function (arg) { return (Is.$array(arg) || Is.$string(arg)) && (arg === "0" || arg.length === 0); }
        ];
        return validations.some(function (item) { return item(arg); });
    };
    return Is;
}());
exports["default"] = Is;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
var __webpack_unused_export__;

__webpack_unused_export__ = ({ value: true });
var Is_1 = __webpack_require__(138);
exports["default"] = Is_1.default;

})();

__webpack_exports__ = __webpack_exports__["default"];
/******/ 	return __webpack_exports__;
/******/ })()
;
});