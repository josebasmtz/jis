require('mocha');
let assert = require('chai').assert;
let jis = require('./')

describe('Jis types test', () => {

    it('$array method', () => {

        assert.equal( jis.$array(''), false, "$array('') must be false");
        assert.equal( jis.$array(1), false, "$array(1) must be false");
        assert.equal( jis.$array([]), true, "$array([]) must be true");

    });

    it('$boolean method', () => {

        assert.equal( jis.$boolean(''), false, "$boolean('') must be false" );
        assert.equal( jis.$boolean(true), true, "$boolean('') must be true" );
        assert.equal( jis.$boolean(false), true, "$boolean('') must be true" );

    })

    it('$function method', () => {

        assert.equal( jis.$function(''), false, "$function('') must be false" )
        assert.equal( jis.$function(() => {}), true, "$function(function(){}) must be true" )

    });

    it('$null method', () => {

        assert.equal(jis.$null(true), false, '$null(true) must be false');
        assert.equal(jis.$null([]), false, '$null([]) must be false');
        assert.equal(jis.$null(null), true, '$null(null) must be true');

    })

    it('$number method', function () {

        assert.equal( jis.$number('12'), false, "$number('12') must be false" )
        assert.equal( jis.$number(12), true, "$number('12') must be true" )

    });

    it('$object method', () => {

        assert.equal( jis.$object(undefined), false, '$object(undefined) must be false' )
        assert.equal( jis.$object({}), true, '$object({}) must be true' )

    });

    it('$string method', () => {

        assert.equal( jis.$string(12), false, '$string(12) must be false' )
        assert.equal( jis.$string([]), false, '$string([]) must be false' )
        assert.equal( jis.$string(""), true, '$string("") must be true' )

    });

    it('$undefined method', function () {

        assert.equal( jis.$undefined({}), false, '$undefined({}) must be false')
        assert.equal( jis.$undefined(undefined), true, '$undefined({}) must be true')

    });

    it('$numeric method', function () {

        assert.equal( jis.$numeric(12), true, '$numeric(12) must be true' );
        assert.equal( jis.$numeric('12'), true, "$numeric('12') must be true" );
        assert.equal( jis.$numeric('-12'), true, "$numeric('-12') must be true" );
        assert.equal( jis.$numeric('+12'), true, "$numeric('+12') must be true" );
        assert.equal( jis.$numeric('12.'), true, "$numeric('12.') must be true" );
        assert.equal( jis.$numeric('12.e5'), true, "$numeric('12.e5') must be true" );
        assert.equal( jis.$numeric('12.E5'), true, "$numeric('12.E5') must be true" );
        assert.equal( jis.$numeric('12.E-5'), true, "$numeric('12.E-5') must be true" );
        assert.equal( jis.$numeric('-12.E-5'), true, "$numeric('-12.E-5') must be true" );
        assert.equal( jis.$numeric('+12.E-5'), true, "$numeric('+12.E-5') must be true" );

        assert.equal( jis.$numeric('12.E-'), false, "$numeric('12.E-') must be false" );
        assert.equal( jis.$numeric('A3B'), false, "$numeric('A3B') must be false" );
        assert.equal( jis.$numeric(undefined), false, "$numeric(undefined) must be false" );
        assert.equal( jis.$numeric(null), false, "$numeric(null) must be false" );

    });

    it('$primitive method', function () {

        assert.equal(jis.$primitive(undefined), true, '$primitive(undefined) must be true')
        assert.equal(jis.$primitive(null), true, '$primitive(null) must be true')
        assert.equal(jis.$primitive("something"), true, '$primitive("something") must be true')
        assert.equal(jis.$primitive(true), true, '$primitive(true) must be true')
        assert.equal(jis.$primitive(false), true, '$primitive(false) must be true')
        assert.equal(jis.$primitive(12), true, '$primitive(12) must be true')
        assert.equal(jis.$primitive(Symbol()), true, '$primitive(Symbol()) must be true')

        assert.equal(jis.$primitive({}), false, '$primitive({}) must be true')
        assert.equal(jis.$primitive([]), false, '$primitive([]) must be true')
        assert.equal(jis.$primitive(new Date()), false, '$primitive(new Date()) must be true')

    });

    it('$empty method', function () {

        assert.equal(jis.$empty(null), true, '$empty(null) must be true')
        assert.equal(jis.$empty(undefined), true, '$empty(undefined) must be true')
        assert.equal(jis.$empty(false), true, '$empty(false) must be true')
        assert.equal(jis.$empty(0), true, '$empty(0) must be true')
        assert.equal(jis.$empty(0.0), true, '$empty(0.0) must be true')
        assert.equal(jis.$empty(""), true, '$empty("") must be true')
        assert.equal(jis.$empty("0"), true, '$empty("0") must be true')
        assert.equal(jis.$empty([]), true, '$empty([]) must be true')

        assert.equal(jis.$empty(true), false, '$empty(true) must be true')
        assert.equal(jis.$empty(12), false, '$empty(12) must be true')
        assert.equal(jis.$empty(12.0), false, '$empty(12.0) must be true')
        assert.equal(jis.$empty("something"), false, '$empty("something") must be true')
        assert.equal(jis.$empty("012"), false, '$empty("012") must be true')
        assert.equal(jis.$empty([1, 2, 3]), false, '$empty([1, 2, 3]) must be true')

    });

    it('"is" method', function () {
        assert.equal(jis.is( [], 'Array' ) , true)
        assert.equal(jis.is( false, 'Boolean' ) , true)
        assert.equal(jis.is( true, 'Boolean' ) , true)
        assert.equal(jis.is( function(){}, 'Function' ) , true)
        assert.equal(jis.is( null, 'Null' ) , true)
        assert.equal(jis.is( 12, 'Number' ) , true)
        assert.equal(jis.is( {}, 'Object' ) , true)
        assert.equal(jis.is( 'Text', 'String' ) , true)
        assert.equal(jis.is( undefined, 'Undefined' ) , true)

        let date = new Date();
        assert.equal(jis.is(date, Date) , true)

        assert.equal(jis.is(/^$/g, RegExp) , true)
        assert.equal(jis.is(/^$/g, 'RegExp') , true)

        assert.equal(jis.is(12, 12), true);
    });

})