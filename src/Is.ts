export default class Is{
    private static callToString(arg:any) {
        return Object.prototype.toString.call(arg)
    }

    static $string(arg:any) {
        return Is.callToString(arg) === '[object String]';
    }

    static is(arg:any, type){
        if(Is.$function(type))
        {
            return arg instanceof type;
        }

        if (Is.$string(type))
        {
            return  Is.callToString(arg) === `[object ${type}]`;
        }

        return arg === type;
    }

    static $array(arg:any){
        return Is.is(arg, 'Array');
    }

    static $null(arg:any){
        return Is.is(arg, 'Null');
    }

    static $number(arg:any){
        return Is.is(arg, 'Number');
    }

    static $object(arg:any){
        return Is.is(arg, 'Object');
    }

    static $undefined(arg:any){
        return Is.is(arg, 'Undefined');
    }

    static $boolean(arg:any){
        return Is.is(arg, 'Boolean');
    }

    static $function(arg:any){
        return Is.callToString(arg) === '[object Function]';
    }

    static objectIsValid(data:object, rules:object) {
        if (!Is.$object(data)) throw new Error('The data parameter must be an Object');
        if (!Is.$object(rules)) throw new Error('The rules parameter must be an Object');

        let $response = true;
        let $keys = Object.getOwnPropertyNames(rules);

        $keys.forEach((key) => {
            let rule = rules[key];
            if (Is.$array(rule)) {
                if (rule.length < 1) return;
                let parcial = false;
                rule.forEach((_rule) => {
                    parcial = parcial || Is.is(data[key], _rule);
                });
                return $response = $response && parcial;
            }
            $response = $response && Is.is(data[key], rule);
        });
        return $response;
    }

    static $numeric(arg: any)
    {
        if (Is.$number(arg))
        {
            return true;
        }

        let $arg = String(arg || '');
        let regex = /^[-+]?(([0-9]+)|([0-9]*(\.[0-9]+))|([0-9]+\.))([Ee]([-+]?[0-9]+))?$/g;

        return regex.test( $arg );
    }

    static $primitive(arg: any)
    {
        let validations = [
            Is.$undefined,
            Is.$null,
            Is.$boolean,
            Is.$number,
            Is.$string,
            (arg) => Is.is(arg, 'Symbol')
        ]

        return validations.some(item => item(arg))
    }

    static $empty(arg: any)
    {
        let validations = [
            Is.$undefined,
            Is.$null,

            (arg) => Is.$boolean(arg) && !arg,
            (arg) => Is.$number(arg) && arg === 0,
            (arg) => (Is.$array(arg) || Is.$string(arg)) && (arg === "0" || (arg as any[]|string).length === 0)
        ]

        return validations.some(item => item(arg))
    }
}