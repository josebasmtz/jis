# J`is`

`jis` is a data type verifier for older browsers. Based in `Object.prototype.toString.call`

The library has some methods to check the data type.
 
 ### Installation
 
 ```shell script
npm install jis
```

You can install this library with `npm` or `yarn`. And you can add in `devDependecies` with

```shell script
npm install jis --dev
```

### Usage

##### Import in ES5 or higher

```js
let jis = require('jis')
```

##### Import in TS

```ts
import jis from 'jis'
```

#### Methods

##### $array

```js

jis.$array( [] ) // true

jis.$array( true ) //false
jis.$array( function() {} ) // false
jis.$array( null ) // false
jis.$array( 12 ) // false
jis.$array( {} ) // false
jis.$array( '' ) // false
jis.$array( undefined ) // false

```

##### $boolean

```js

jis.$boolean( true ) // true
jis.$boolean( false ) // true

jis.$boolean( [] ) //false
jis.$boolean( function() {} ) // false

// ...

```

##### $function

```js

jis.$function( function() {} ) // true

jis.$function( 12 ) // false
jis.$function( {} ) // false
jis.$function( '' ) // false

// ...

```

##### $null

```js

jis.$null( null ) // true

jis.$null( {} ) // false
jis.$null( '' ) // false
jis.$null( undefined ) // false

// ...

```

##### $number

```js

jis.$number( 12 ) // true

jis.$number( function() {} ) // false
jis.$number( null ) // false
jis.$number( undefined ) // false

// ...

```

##### $object

```js

jis.$object( {} ) // true

jis.$object( [] ) // false
jis.$object( true ) //false
jis.$object( 12 ) // false

// ...

```

##### $string

```js

jis.$string( 'Some text' ) // true

jis.$string( [] ) // false
jis.$string( true ) //false
jis.$string( undefined ) // false

// ...

```

##### $undefined

```js

jis.$undefined( undefined ) // true

jis.$undefined( [] ) // false
jis.$undefined( {} ) // false
jis.$undefined( '' ) // false

// ...

```

##### $numeric

Check if the argument is a number or number string (including exponential)

```js

jis.$numeric( 12 ) // true
jis.$numeric( '12' ) // true
jis.$numeric( '-12' ) // true
jis.$numeric( '+12' ) // true
jis.$numeric( '12.' ) // true
jis.$numeric( '12.e5' ) // true
jis.$numeric( '12.E5' ) // true
jis.$numeric( '12.E-5' ) // true
jis.$numeric( '-12.E-5' ) // true
jis.$numeric( '+12.E-5' ) // true

jis.$numeric( '12.E-' ) // false
jis.$numeric( 'A3B' ) // false
jis.$numeric( undefined ) // false
jis.$numeric( null ) // false

```

##### $primitive

Check if the argument is primitive type

```js

jis.$primitive(undefined) // true
jis.$primitive(null) // true
jis.$primitive("something") // true
jis.$primitive(true) // true
jis.$primitive(false) // true
jis.$primitive(12) // true
jis.$primitive(Symbol()) // true

jis.$primitive({}) // false
jis.$primitive([]) // false
jis.$primitive(new Date()) // false

```

##### $empty

```js

jis.$empty(null) // true
jis.$empty(undefined) // true
jis.$empty(false) // true
jis.$empty(0) // true
jis.$empty(0.0) // true
jis.$empty("") // true
jis.$empty("0") // true
jis.$empty([]) // true

jis.$empty(true) // false
jis.$empty(12) // false
jis.$empty(12.0) // false
jis.$empty("something") // false
jis.$empty("012") // false
jis.$empty([1, 2, 3]) // false

```

##### is

This method is the library core. Have more options.

```js

jis.is( [], 'Array' ) // true
jis.is( false, 'Boolean' ) // true
jis.is( true, 'Boolean' ) // true
jis.is( function(){}, 'Function' ) // true
jis.is( null, 'Null' ) // true
jis.is( 12, 'Number' ) // true
jis.is( {}, 'Object' ) // true
jis.is( 'Text', 'String' ) // true
jis.is( undefined, 'Undefined' ) // true

let date = new Date();
jis.is(date, Date) // true

jis.is(/^$/g, RegExp) // true
jis.is(/^$/g, 'RegExp') // true

jis.is(12, 12) // true
jis.is(12, 13) // false

// ... experiment with this method :D

```