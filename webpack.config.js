const path = require('path')
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    mode: 'production',
    entry: {
        'jis': './src/main.ts',
        'jis.min': './src/main.ts'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js',
        libraryTarget: 'umd',
        library: {
            name: 'jis',
            type: 'global'
        },
        umdNamedDefine: true,
        globalObject: "this",
        libraryExport: "default"
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    // devtool: 'source-map',
    module: {
        rules: [{
            test: /\.tsx?$/,
            loader: 'ts-loader',
            exclude: /node_modules/
        }]
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                include: /\.min\.js$/,
                extractComments: false,
            })
        ]
    }
}